import unittest
from Coche import Coche

coche1 = Coche("rojo", "ford", "fiesta", "9123")
class TestCocheCase(unittest.TestCase):
    def test_acelera(self):
        coche1.velocidad = 30
        vf = coche1.acelera(40)
        self.assertEqual(vf, 70)
    def test_acelera_mal(self):
        coche1.velocidad = 30
        vf = coche1.acelera(40)
        self.assertNotEqual(vf, 60)
    def test_frena(self):
        coche1.velocidad = 30
        vf = coche1.frena(20)
        self.assertEqual(vf, 10)
    def test_frena_mal(self):
        coche1.velocidad = 30
        vf = coche1.frena(40)
        self.assertNotEqual(vf, 60)


if __name__ == "__main__":
    unittest.main()
